-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: srax
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `heroes`
--

DROP TABLE IF EXISTS `heroes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `heroes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT '1',
  `class` varchar(45) DEFAULT NULL,
  `weapon` varchar(45) DEFAULT NULL,
  `stat_strength` int(11) DEFAULT '0',
  `stat_intelligence` int(11) DEFAULT '0',
  `stat_dexterity` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1',
  `race` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `heroes`
--

LOCK TABLES `heroes` WRITE;
/*!40000 ALTER TABLE `heroes` DISABLE KEYS */;
INSERT INTO `heroes` VALUES (1,'Khazun','nuzahK',1,'Paladin','Sword',15,10,3,'2019-10-14 04:52:08','2019-10-14 05:17:55',1,NULL),(2,'Khazun','nuzahK',1,'Paladin','Sword',8,8,8,'2019-10-14 04:52:20','2019-10-14 08:24:55',0,NULL),(3,'Khazun','nuzahK',1,'Paladin','Sword',12,5,8,'2019-10-14 04:57:42','2019-10-14 04:57:42',1,NULL),(4,'Khazun','nuzahK',1,'Paladin','Sword',7,12,9,'2019-10-14 04:59:17','2019-10-14 04:59:17',1,NULL),(5,'Khazun','nuzahK',1,'Paladin','Sword',6,14,12,'2019-10-14 08:30:51','2019-10-14 08:30:51',1,NULL),(6,'Khazun','nuzahK',1,'Paladin','Sword',9,10,7,'2019-10-14 08:31:08','2019-10-14 08:31:08',1,NULL),(7,'Khazun','nuzahK',1,'Paladin','Sword',7,13,13,'2019-10-14 08:33:59','2019-10-14 08:33:59',1,NULL),(8,'Khazun','nuzahK',1,'Paladin','Sword',11,12,8,'2019-10-14 08:34:15','2019-10-14 08:34:15',1,NULL),(9,'Khazun','nuzahK',1,'Paladin','Sword',4,13,11,'2019-10-14 08:34:22','2019-10-14 08:34:22',1,NULL),(10,'Khazun','nuzahK',1,'Paladin','Sword',12,8,6,'2019-10-14 08:35:49','2019-10-14 08:35:49',1,NULL),(11,'Khazun','nuzahK',1,'Paladin','Sword',15,11,11,'2019-10-14 08:36:16','2019-10-14 08:36:16',1,NULL),(12,'Khazun','nuzahK',1,'Paladin','Sword',6,10,9,'2019-10-14 08:39:28','2019-10-14 08:39:28',1,NULL),(13,'Khazun','nuzahK',1,'Paladin','Sword',11,9,5,'2019-10-14 08:39:28','2019-10-14 08:39:28',1,NULL),(14,'Khazun','something',1,'Paladin','Sword',9,8,9,'2019-10-14 08:39:48','2019-10-14 08:39:48',1,NULL),(15,'Khazun',NULL,1,'Paladin','Sword',12,12,7,'2019-10-14 08:39:48','2019-10-14 08:39:48',1,NULL),(16,'Khazun','something',1,'Paladin','Sword',10,9,5,'2019-10-14 08:41:25','2019-10-14 08:41:25',1,NULL),(17,'Khazun','something',1,'Paladin','Sword',9,9,6,'2019-10-14 08:41:25','2019-10-14 08:41:25',1,NULL),(18,'Khazun','nuzahK',1,'Paladin','Sword',9,10,9,'2019-10-14 08:41:43','2019-10-14 08:41:43',1,NULL),(19,'Khazun','nuzahK',1,'Paladin','Sword',6,10,9,'2019-10-14 08:54:32','2019-10-14 08:54:32',1,'Elf'),(20,'Khazun','nuzahK',1,'Paladin','Sword',6,3,12,'2019-10-14 08:57:28','2019-10-14 08:57:28',1,'Elf'),(21,'Khazun','nuzahK',1,'Paladin','Sword',10,9,8,'2019-10-14 08:58:59','2019-10-14 08:58:59',1,'Elf'),(22,'Khazun','nuzahK',1,'Paladin','Sword',10,4,5,'2019-10-14 08:59:24','2019-10-14 08:59:24',1,'Elf'),(23,'Khazun','nuzahK',1,'Paladin','Sword',7,6,7,'2019-10-14 09:00:16','2019-10-14 09:00:16',1,'Elf'),(24,'Khazun','nuzahK',1,'Paladin','Sword',10,5,6,'2019-10-14 09:00:27','2019-10-14 09:00:27',1,'Elf'),(25,'Khazun','nuzahK',1,'Paladin','Sword',11,7,14,'2019-10-14 09:02:56','2019-10-14 09:02:56',1,'Elf'),(26,'Khazun','nuzahK',1,'Paladin','Sword',8,11,15,'2019-10-14 09:05:17','2019-10-14 09:05:17',1,'Elf'),(27,'Khazun','nuzahK',1,'Paladin','Sword',5,12,4,'2019-10-14 09:05:21','2019-10-14 09:05:21',1,'Elf'),(28,'Khazun','nuzahK',1,'Paladin','Sword',5,8,14,'2019-10-14 09:06:56','2019-10-14 09:06:56',1,'Elf'),(29,'Khazun','nuzahK',1,'Paladin','Sword',14,7,14,'2019-10-14 09:07:24','2019-10-14 09:07:24',1,'Elf'),(30,'Khazun','nuzahK',1,'Paladin','Sword',8,12,11,'2019-10-14 09:07:50','2019-10-14 09:07:50',1,'Elf'),(31,'Khazun','nuzahK',1,'Paladin','Sword',12,11,9,'2019-10-14 09:08:30','2019-10-14 09:08:30',1,'Elf'),(32,'Khazun','nuzahK',1,'Paladin','Sword',9,9,10,'2019-10-14 09:10:40','2019-10-14 09:10:40',1,'Elf'),(33,'Khazun','nuzahK',1,'Paladin','Sword',9,11,10,'2019-10-14 09:12:02','2019-10-14 09:12:02',1,'Elf'),(34,'Khazun','nuzahK',1,'Paladin','Sword',10,9,11,'2019-10-14 09:14:36','2019-10-14 09:14:36',1,'Elf'),(35,'Khazun','nuzahK',1,'Paladin','Sword',9,11,10,'2019-10-14 09:15:09','2019-10-14 09:15:09',1,'Elf'),(36,'Khazun','nuzahK',1,'Paladin','Sword',11,10,9,'2019-10-14 09:15:47','2019-10-14 09:15:47',1,'Elf'),(37,'Khazun','nuzahK',1,'Paladin','Sword',9,14,4,'2019-10-14 09:16:32','2019-10-14 09:16:32',1,'Elf'),(38,'Khazun','nuzahK',1,'Barbarian','Sword',12,8,10,'2019-10-14 09:16:32','2019-10-14 09:16:32',1,'Elf'),(39,'Khazun','nuzahK',1,'Paladin','Sword',11,6,5,'2019-10-14 09:17:49','2019-10-14 09:17:49',1,'Elf'),(40,'Khazun','nuzahK',1,'Paladin','Sword',4,8,10,'2019-10-14 09:18:15','2019-10-14 09:18:15',1,'Elf'),(41,'Khazun','nuzahK',1,'Paladin','Sword',9,12,13,'2019-10-14 09:20:02','2019-10-14 09:20:02',1,'Elf'),(42,'Khazun','nuzahK',1,'Paladin','Sword',9,7,13,'2019-10-14 09:20:52','2019-10-14 09:20:52',1,'Elf'),(43,'Khazun','nuzahK',1,'Paladin','Sword',8,12,9,'2019-10-14 09:22:18','2019-10-14 09:22:18',1,'Elf'),(44,'Khazun','nuzahK',1,'Paladin','Sword',4,13,7,'2019-10-14 09:22:45','2019-10-14 09:22:45',1,'Elf'),(45,'Khazun','nuzahK',1,'Paladin','Sword',8,12,16,'2019-10-14 09:23:03','2019-10-14 09:23:03',1,'Elf'),(46,'Khazun','nuzahK',1,'Paladin','Sword',7,9,9,'2019-10-14 09:23:19','2019-10-14 09:23:19',1,'Elf'),(47,'Khazun','nuzahK',1,'Paladin','Sword',8,11,7,'2019-10-14 09:23:42','2019-10-14 09:23:42',1,'Elf'),(48,'Khazun','nuzahK',1,'Paladin','Sword',8,6,4,'2019-10-14 09:23:49','2019-10-14 09:23:49',1,'Elf'),(49,'Khazun','nuzahK',1,'Paladin','Sword',7,7,10,'2019-10-14 09:24:56','2019-10-14 09:24:56',1,'Elf'),(50,'Khazun','nuzahK',1,'Paladin','Sword',13,9,11,'2019-10-14 09:25:19','2019-10-14 09:25:19',1,'Elf'),(51,'Khazun','nuzahK',1,'Paladin','Sword',10,9,11,'2019-10-14 09:25:29','2019-10-14 09:25:29',1,'Elf'),(52,'Khazun','nuzahK',1,'Paladin','Sword',8,8,11,'2019-10-14 09:25:34','2019-10-14 09:25:34',1,'Elf'),(53,'Khazun',NULL,1,'Wizard','Sword',9,9,3,'2019-10-14 09:25:34','2019-10-14 09:25:34',1,'Dragonborn'),(54,'Khazun','nuzahK',1,'Paladin','Sword',9,8,10,'2019-10-14 09:25:44','2019-10-14 09:25:44',1,'Elf'),(55,'Khazun','nuzahK',1,'Paladin','Sword',11,11,13,'2019-10-14 09:25:55','2019-10-14 09:25:55',1,'Elf'),(56,'Khazun','nuzahK',1,'Paladin','Sword',15,3,13,'2019-10-14 09:26:07','2019-10-14 09:26:07',1,'Elf'),(57,'Khazun','nuzahK',1,'Paladin','Sword',7,5,9,'2019-10-14 09:26:34','2019-10-14 09:26:34',1,'Elf'),(58,'Khazun','nuzahK',1,'Paladin','Sword',11,4,5,'2019-10-14 09:26:58','2019-10-14 09:26:58',1,'Elf'),(59,'Khazun','nuzahK',1,'Paladin','Sword',7,9,13,'2019-10-14 09:28:00','2019-10-14 09:28:00',1,'Elf'),(60,'Khazun','nuzahK',1,'Paladin','Sword',12,9,8,'2019-10-14 09:31:31','2019-10-14 09:31:31',1,'Elf'),(61,'Khazun','nuzahK',1,'Paladin','Sword',5,14,10,'2019-10-14 09:31:44','2019-10-14 09:31:44',1,'Elf'),(62,'Khazun','nuzahK',1,'Paladin','Sword',7,14,4,'2019-10-14 09:32:29','2019-10-14 09:32:29',1,'Elf'),(63,'Khazun','nuzahK',1,'Paladin','Sword',8,9,9,'2019-10-14 09:32:38','2019-10-14 09:32:38',1,'Elf'),(64,'Khazun','nuzahK',1,'Paladin','Sword',10,7,8,'2019-10-14 09:32:53','2019-10-14 09:32:53',1,'Elf'),(65,'Khazun','nuzahK',1,'Paladin','Sword',6,7,6,'2019-10-14 09:33:09','2019-10-14 09:33:09',1,'Elf'),(66,'Khazun','nuzahK',1,'Paladin','Sword',11,5,12,'2019-10-14 09:33:31','2019-10-14 09:33:31',1,'Elf'),(67,'Khazun','nuzahK',1,'Paladin','Sword',9,13,11,'2019-10-14 09:34:15','2019-10-14 09:34:15',1,'Elf'),(68,'Khazun','nuzahK',1,'Paladin','Sword',7,6,6,'2019-10-14 09:34:21','2019-10-14 09:34:21',1,'Elf'),(69,'Khazun','nuzahK',1,'Paladin','Sword',12,3,5,'2019-10-14 09:34:33','2019-10-14 09:34:33',1,'Elf'),(70,'Khazun','nuzahK',1,'Wizard','Sword',6,5,7,'2019-10-14 09:34:33','2019-10-14 09:34:33',1,'Elf'),(71,'Khazun','nuzahK',1,'Paladin','Sword',13,8,8,'2019-10-14 09:35:57','2019-10-14 09:35:57',1,'Elf'),(72,'Khazun','nuzahK',1,'Wizard','Sword',6,8,4,'2019-10-14 09:35:57','2019-10-14 09:35:57',1,'Elf'),(73,'Khazun','nuzahK',1,'Paladin','Sword',14,9,14,'2019-10-14 09:37:19','2019-10-14 09:37:19',1,'Elf'),(74,'Khazun','nuzahK',1,'Paladin','Sword',7,10,8,'2019-10-14 09:37:50','2019-10-14 09:37:50',1,'Elf'),(75,'Khazun','nuzahK',1,'Wizard','Sword',9,12,9,'2019-10-14 09:37:50','2019-10-14 09:37:50',1,'Elf'),(76,'Khazun','nuzahK',1,'Paladin','Sword',12,7,5,'2019-10-14 09:38:25','2019-10-14 09:38:25',1,'Elf'),(77,'Khazun','nuzahK',1,'Paladin','Sword',9,11,9,'2019-10-14 09:38:44','2019-10-14 09:38:44',1,'Elf'),(78,'Khazun','nuzahK',1,'Paladin','Sword',12,11,4,'2019-10-14 09:39:39','2019-10-14 09:39:39',1,'Elf'),(79,'Khazun','nuzahK',1,'Thief','Sword',10,8,9,'2019-10-14 09:39:39','2019-10-14 09:39:39',1,'Elf'),(80,'Khazun','nuzahK',1,'Paladin','Sword',6,4,8,'2019-10-14 09:39:53','2019-10-14 09:39:53',1,'Elf'),(81,'Khazun','nuzahK',1,'Paladin','Sword',6,10,8,'2019-10-14 09:40:22','2019-10-14 09:40:22',1,'Elf'),(82,'Khazun','nuzahK',1,'Paladin','Sword',6,12,13,'2019-10-14 09:40:42','2019-10-14 09:40:42',1,'Elf');
/*!40000 ALTER TABLE `heroes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_10_12_232116_create_table_pokemons',1),(4,'2019_10_12_235714_create_table_pokemon',2),(5,'2019_10_13_000754_create_table_hero',3),(6,'2019_10_13_000759_create_table_monster',3),(7,'2019_10_13_001044_create_table_heros',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monsters`
--

DROP TABLE IF EXISTS `monsters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monsters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` text,
  `name` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT '1',
  `race` varchar(45) DEFAULT NULL,
  `abilities` varchar(45) DEFAULT NULL,
  `stat_strength` int(11) DEFAULT '0',
  `stat_intelligence` int(11) DEFAULT '0',
  `stat_dexterity` int(11) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monsters`
--

LOCK TABLES `monsters` WRITE;
/*!40000 ALTER TABLE `monsters` DISABLE KEYS */;
INSERT INTO `monsters` VALUES (1,'tets url ','angel',1,'Beholder','Shadow Ball,',23,22,12,'2019-10-14 10:12:52','2019-10-13 19:12:12',0),(2,'some link','BHE\'',1,'Beholder','Shadow Ball',39,27,26,'2019-10-14 15:08:00','2019-10-14 15:08:00',1),(3,'some link','BHE\'',1,'Beholder','Shadow Ball',38,22,40,'2019-10-14 15:08:22','2019-10-14 15:08:22',1),(4,'some link','BHE\'',1,'Beholder','Shadow Ball',29,23,32,'2019-10-14 15:08:59','2019-10-14 15:08:59',1),(5,'some link','D\'',1,'Dragons','Aerial Ace',45,27,31,'2019-10-14 15:09:00','2019-10-14 15:09:00',1),(6,'some link','\'',1,'Gelatinous Cube','Surf',38,21,33,'2019-10-14 15:09:00','2019-10-14 15:09:00',1),(7,'some link','BHE\'',1,'Beholder','Shadow Ball',43,37,30,'2019-10-14 15:10:57','2019-10-14 15:10:57',1),(8,'some link','D\'',1,'Dragons','Aerial Ace',28,29,27,'2019-10-14 15:10:57','2019-10-14 15:10:57',1),(9,'some link','BHE\'',1,'Beholder','Shadow Ball',32,38,40,'2019-10-14 15:11:10','2019-10-14 15:11:10',1),(10,'some link','BHE\'',1,'Beholder','Shadow Ball',45,32,33,'2019-10-14 15:16:10','2019-10-14 15:16:10',1),(11,'some link','D\'',1,'Dragons','Aerial Ace',38,28,27,'2019-10-14 15:16:10','2019-10-14 15:16:10',1),(12,'some link','\'',1,'Gelatinous Cube','Surf',32,30,37,'2019-10-14 15:16:10','2019-10-14 15:16:10',1),(13,'some link','BHE\'',1,'Beholder','Shadow Ball',39,53,29,'2019-10-14 15:17:01','2019-10-14 15:17:01',1),(14,'some link','D\'',1,'Dragons','Aerial Ace',41,42,51,'2019-10-14 15:17:01','2019-10-14 15:17:01',1),(15,'some link','\'',1,'Gelatinous Cube','Surf',35,42,24,'2019-10-14 15:17:01','2019-10-14 15:17:01',1),(16,'some link','BHE\'',1,'Beholder','Shadow Ball',37,37,29,'2019-10-14 15:17:19','2019-10-14 15:17:19',1),(17,'some link','D\'',1,'Dragons','Aerial Ace',25,33,31,'2019-10-14 15:17:19','2019-10-14 15:17:19',1),(18,'some link','\'',1,'Gelatinous Cube','Surf',39,27,39,'2019-10-14 15:17:19','2019-10-14 15:17:19',1),(19,'some link','BHE\'',1,'Beholder','Shadow Ball',36,31,29,'2019-10-14 15:19:39','2019-10-14 15:19:39',1),(20,'some link','D\'',1,'Dragons','Aerial Ace',28,35,30,'2019-10-14 15:19:39','2019-10-14 15:19:39',1),(21,'some link','\'',1,'Gelatinous Cube','Surf',38,46,30,'2019-10-14 15:19:39','2019-10-14 15:19:39',1),(22,'some link','BHE\'',1,'Beholder','Shadow Ball',35,41,28,'2019-10-14 15:20:20','2019-10-14 15:20:20',1),(23,'some link','D\'',1,'Dragons','Aerial Ace',34,31,24,'2019-10-14 15:20:20','2019-10-14 15:20:20',1),(24,'some link','\'',1,'Gelatinous Cube','Surf',43,36,31,'2019-10-14 15:20:20','2019-10-14 15:20:20',1),(25,'some link manual','Example name',1,'Beholder','Shadow Ball',27,33,45,'2019-10-14 15:22:35','2019-10-14 15:22:35',1),(26,'some link manual','Example name',1,'Beholder','Shadow Ball',39,33,38,'2019-10-14 15:24:01','2019-10-14 15:24:01',1);
/*!40000 ALTER TABLE `monsters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-14  1:25:54
