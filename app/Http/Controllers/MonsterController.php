<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Monster;

class MonsterController extends Controller
{
    private $monsterRaces   = null;
    private $monsterPowers  = null;

    public function __construct()
    {
        $this->monsterRaces = collect(
            ["Beholder", "Mind Flayer", "Drow", "Dragons", "Owlbear", "Bulette", "Rust Monster", "Gelatinous Cube",
                "Hill Giant", "Stone Giant", "Frost Giant", "Fire Giant", "Cloud Giant", "Storm Giant",
            "Displacer Beast", "Githyanki", "Kobold", "Kuo-Toa", "Lich", "Orc", "Slaad", "Umber Hulk", "Yuan-ti"]);

        $this->monsterPowers = collect(
            ["Shadow Ball", "Aerial Ace", "Giga Drain", "Thunderbolt", "Earthquake", "Crunch", "Double Team",
                "Psychic", "Ice Beam", "Surf"]);
    }

    public function index()
    {
        $data = Monster::all()->where('status', 1);
        return response()->json($data);
    }

    public function store(Request $request)
    {
        try {
            // Validate that data is received
            if (!count($request->json()->all()))
                throw new Exception("Didn't receive data");
            // Create variable with the data
            $data = $request->json()->all();

            // is Valid race?
            if ($this->isValidValue($data['race'], $this->monsterRaces))
                throw new Exception("The race is not valid");

            /// Stats
            $data['stat_strength']          = $this->generateRandomStat();
            $data['stat_intelligence']      = $this->generateRandomStat();
            $data['stat_dexterity']         = $this->generateRandomStat();

            // Run the validation
            $data = $this->validMonster($data);

            // Save the new hero once is valid
            $monster = new Monster;
            $monster->picture              = $data['picture'];
            $monster->name                 = $data['name'];
            $monster->race                 = $data['race'];
            $monster->level                = 1;
            $monster->abilities            = $data['abilities'];
            $monster->stat_strength        = $data['stat_strength'];
            $monster->stat_intelligence    = $data['stat_intelligence'];
            $monster->stat_dexterity       = $data['stat_dexterity'];
            $monster->save();

            // Prepare response
            $response['error']  = false;
            $response['monster']   = $data;
            $response['msg']    = "Monster created successfully";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }

    }

    public function show($id)
    {
        try{
            $monster = Monster::find($id);
            if ($monster == null)
                throw new Exception("Can't find the monster");
            $response['error']  = false;
            $response['monster']   = $monster;
            $response['msg']    = "Monster information";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            // Validate that data is received
            $data='';
            if (!count($request->json()->all()))
                throw new Exception("Didn't receive data");
            // Create variable with the data
            $data = $request->json()->all();
            // Run the validation
            $data = $this->validMonster($data);

            /// Stats, valid that the received stats are numeric
            if (!is_numeric($data['stat_strength']))
                throw new Exception("Stat Strength have a non numeric value");
            if (!is_numeric($data['stat_intelligence']))
                throw new Exception("Stat Intelligence have a non numeric value");
            if (!is_numeric($data['stat_dexterity']))
                throw new Exception("Stat Dexterity have a non numeric value");

            // Validate that the received values are valid
            if ($data['stat_strength'] < 0 ||$data['stat_strength'] > 18)
                throw new Exception("Value of Stat Strength is invalid");
            if ($data['stat_intelligence'] < 0 ||$data['stat_intelligence'] > 18)
                throw new Exception("Value of Stat Intelligence is invalid");
            if ($data['stat_dexterity'] < 0 ||$data['stat_dexterity'] > 18)
                throw new Exception("Value of Stat Dexterity is invalid");

            $monster = Monster::find($id);
            $monster->picture              = $data['picture'];
            $monster->name                 = $data['name'];
            $monster->race                 = $data['race'];
            $monster->level                = 1;
            $monster->abilities            = $data['abilities'];
            $monster->stat_strength        = $data['stat_strength'];
            $monster->stat_intelligence    = $data['stat_intelligence'];
            $monster->stat_dexterity       = $data['stat_dexterity'];
            $monster->save();

            // Prepare response
            $response['error']  = false;
            $response['hero']   = $monster;
            $response['msg']    = "Hero updated successfully";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }

    }

    public function destroy($id)
    {
        try{
            $monster = Monster::find($id);
            if ($monster == null)
                throw new Exception("Can't find the monster");

            $monster->status = 0;
            $monster->save();
            $response['error']  = false;
            $response['hero']   = $monster;
            $response['msg']    = "Hero deleted";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }
    }

    public function isValidValue($needle, $haystack)
    {
        foreach ($haystack as $item){
            if ($needle == $item)
                return false;
        }
        return true;
    }

    public  function generateRandomStat(){
        $values = [];
        for ($x = 0; $x < 4; $x++){
            $value = rand(6, 20);
            if (!in_array($value, $values)){
                $values[] = $value;
            }else{
                $x--;
            }
        }
        sort($values);
        array_pop($values);
        return array_sum($values);
    }

    public function validMonster($data){

        if ($data['isRandom']){
            $name = strtoupper($data['race']);
            // patch the name
            // - Every A char in the name will be replaced with O
            $name = str_replace("A", "O", $name);
            // - Every U char in the name will be replaced with A
            $name = str_replace("U", "A", $name);
            // - Every I will add another I right next to it
            $name = str_replace("I", "II", $name);
            // Every E char in the name will change position with the next character on the right
            $name2 = "";
            $name = str_split($name);
            for ($x = 0; $x < sizeof($name); $x++){
                if ($name[$x] == "E"){
                    if (($x + 1) < sizeof($name)){
                        $name2.=$name[$x + 1];
                        $name2.=$name[$x];
                        $x = $x +1;
                    }else{
                        $name2.=$name[$x];
                    }
                }else{
                    $name2.=$name[$x];
                }
            }
            $name = $name2;
            // Every O char in the name will add an R on the left side of it
            $name = str_replace("O", "RO", $name);
            // Every R and G chars will change to “an apostrophe and a space” ( or \’\s in regular expression)
            $name = preg_replace("/(R|G)/", "' ", $name);
            // Non alphanumeric chars (like spaces or dashes) will make everything on the right to be ignored
            $name = explode(" ", $name)[0];
            $name = explode("_", $name)[0];
            $data['name'] = $name;
        }else{
            if ($data['name'] == "")
                throw new Exception("Name is mandatory");
        }



        // Abilities Limitations
        if ($data['abilities'] == "")
            throw new Exception("Didn't receive abilities");

        $abilities = explode(",", $data['abilities']);
        foreach ($abilities as $a){
            // Shadow Ball can only be used by Beholder and Mind Flayer
            if ($a == "Shadow Ball")
                if (!in_array($data['race'], ["Beholder", "Mind Flayer"] ))
                    throw new Exception("Shadow Ball can only be used by Beholder and Mind Flayer");

            // Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant, Umber Hulk
            if ($a == "Aerial Ace")
                if (!in_array($data['race'], ["Beholder","Owlbear", "Dragons", "Cloud Gigant", "Storm Gigant", "Umber Hulk"]))
                    throw new Exception("Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant, Umber Hulk");

            // Surf can only be used by Yuan-ti, Gelatinous Cube, Drow
            if ($a == "Surf")
                if (!in_array($data['race'], ["Beholder","Yuan-ti", "Gelatinous Cube", "Drow"]))
                    throw new Exception("Surf can only be used by Yuan-ti, Gelatinous Cube and Drow");

            // Giga Drain can only be used by Mind Flyer
            if ($a == "Giga Drain")
                if (!in_array($data['race'], ["Beholder", "Mind Flyer"]))
                    throw new Exception("Giga Drain can only be used by Mind Flyer");

        }

        // Kobold can only use Double Team and Crunch
        if ($data['race'] == "Kobold"){
            foreach ($abilities as $a){
                if ($a != "Double Team" && $a != "Crunch")
                    throw new Exception("Kobold can only use Double Team and Crunch");
            }
        }

        // If reach this line, means that the hero object is correct
        return $data;
    }
}
