<?php /** @noinspection ALL */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Custom 
use Exception;
use App\Hero;


class HeroController extends Controller
{
    private $heroFirstName  = null;
    private $heroLastName   = null;
    private $heroRaces      = null;
    private $heroClasses    = null;
    private $heroWeapons    = null;

    public function __construct()
    {
        $this->heroFirstName = collect(
            ["Bheizer", "Khazun", "Grirgel", "Murgil", "Edraf", "En", 
            "Grognur", "Grum", "Surhathion", "Lamos", "Melmedjad", 
            "Shouthes", "Che", "Jun", "Rircurtun", "Zelen"]);

        $this->heroLastName = collect(
            ["Nema", "Dhusher", "Burningsun", "Hawkglow", "Nav", "Kadev", "Lightkeeper", "Heartdancer",
            "Fivrithrit", "Suechit", "Tuldethatvo", "Vrovakya", 
            "Hiao", "Chiay", "Hogoscu", "Vedrimor"]);

        $this->heroClasses = collect(
            ["Paladin", "Ranger", "Barbarian", "Wizard", "Cleric", "Warrior", "Thief"]);
        
        $this->heroRaces = collect(
            ["Human", "Elf", "Halfling", "Dwarf", "Half-orc", "Half-elf", "Dragonborn"]);
        
        $this->heroWeapons = collect(
            ["Sword", "Dagger", "Hammer", "Bow and Arrows", "Staff"]);
    }

    public function index()
    {
        $data = Hero::all()->where('status', 1);
        return response()->json($data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        try {
            // Validate that data is received
            $data='';
            if (!count($request->json()->all())) 
                throw new Exception("Didn't receive data");
            // Create variable with the data
            $data = $request->json()->all();
            // Run the validation
            $data = $this->validHero($data);

            /// Stats
            $data['stat_strength']          = $this->generateRandomStat();
            $data['stat_intelligence']      = $this->generateRandomStat();
            $data['stat_dexterity']         = $this->generateRandomStat();

            // Save the new hero once is valid
            $hero = new Hero;
            $hero->first_name           = $data['first_name'];
            $hero->last_name            = $data['last_name'];
            $hero->race                 = $data['race'];
            $hero->level                = 1;
            $hero->class                = $data['class'];
            $hero->weapon               = $data['weapon'];
            $hero->stat_strength        = $data['stat_strength'];
            $hero->stat_intelligence    = $data['stat_intelligence'];
            $hero->stat_dexterity       = $data['stat_dexterity'];
            $hero->save();

            // Prepare response
            $response['error']  = false;
            $response['hero']   = $data;
            $response['msg']    = "Hero created successfully";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }

    }

    public function show($id)
    {
        try{
            $hero = Hero::find($id);
            if ($hero == null)
                throw new Exception("Can't find the hero");
            $response['error']  = false;
            $response['hero']   = $hero;
            $response['msg']    = "Hero information";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        try {
            // Validate that data is received
            $data='';
            if (!count($request->json()->all()))
                throw new Exception("Didn't receive data");
            // Create variable with the data
            $data = $request->json()->all();
            // Run the validation
            $data = $this->validHero($data);

            /// Stats, valid that the received stats are numeric
            if (!is_numeric($data['stat_strength']))
                throw new Exception("Stat Strength have a non numeric value");
            if (!is_numeric($data['stat_intelligence']))
                throw new Exception("Stat Intelligence have a non numeric value");
            if (!is_numeric($data['stat_dexterity']))
                throw new Exception("Stat Dexterity have a non numeric value");

            // Validate that the received values are valid
            if ($data['stat_strength'] < 0 ||$data['stat_strength'] > 18)
                throw new Exception("Value of Stat Strength is invalid");
            if ($data['stat_intelligence'] < 0 ||$data['stat_intelligence'] > 18)
                throw new Exception("Value of Stat Intelligence is invalid");
            if ($data['stat_dexterity'] < 0 ||$data['stat_dexterity'] > 18)
                throw new Exception("Value of Stat Dexterity is invalid");

            $hero = Hero::find($id);

            $hero->first_name           = $data['first_name'];
            $hero->last_name            = $data['last_name'];
            $hero->race                 = $data['race'];
            $hero->level                = 1;
            $hero->class                = $data['class'];
            $hero->weapon               = $data['weapon'];
            $hero->stat_strength        = $data['stat_strength'];
            $hero->stat_intelligence    = $data['stat_intelligence'];
            $hero->stat_dexterity       = $data['stat_dexterity'];
            $hero->save();

            // Prepare response
            $response['error']  = false;
            $response['hero']   = $hero;
            $response['msg']    = "Hero updated successfully";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }

    }

    public function destroy($id)
    {
        try{
            $hero = Hero::find($id);
            if ($hero == null)
                throw new Exception("Can't find the hero");

            $hero->status = 0;
            $hero->save();
            $response['error']  = false;
            $response['hero']   = $hero;
            $response['msg']    = "Hero deleted";
            return response()->json($response);
        } catch (Exception $e) {
            $response['error']  = true;
            $response['msg']    = $e->getMessage();
            return response()->json($response);
        }
    }

    public function mandatoryFields($data)
    {
        $msg = 1;
        if ($data['first_name'] == "") {
            $msg = "First name is empty";
        }
        if ($data['race'] == "") {
            $msg = "Race is empty";
        }
        if ($data['weapon'] == "") {
            $msg = "Weapon is empty";
        }
        if ($data['stat_strength'] < 0 || $data['stat_strength'] > 100) {
            $msg = "Stat strength have forbidden value";
        }
        if ($data['stat_intelligence'] < 0 || $data['stat_intelligence'] > 100) {
            $msg = "Stat intelligence have forbidden value";
        }
        if ($data['stat_dexterity'] < 0 || $data['stat_dexterity'] > 100) {
            $msg = "Stat dexerity have forbidden value";
        }
        return $msg;
    }

    public function isValidValue($needle, $haystack)
    {
        foreach ($haystack as $item){
            if ($needle == $item)
                return false;
        }
        return true;
    }

    public  function generateRandomStat(){
        $values = [];
        for ($x = 0; $x < 4; $x++){
            $values[] = rand(1,6);
        }
        sort($values);
        array_pop($values);
        return array_sum($values);
    }

    public function validHero($data){
        // Validate the mandatory fields
        $mandatoryFields = $this->mandatoryFields($data);
        if ($mandatoryFields != 1)
            throw new Exception($mandatoryFields);

        // Validate name
        if ($this->isValidValue($data['first_name'], $this->heroFirstName))
            throw new Exception("First name isn't valid");

        // Validate race
        if ($this->isValidValue($data['race'], $this->heroRaces)) {
            throw new Exception("Race isn't valid");
        }
        // Validate class
        if ($this->isValidValue($data['class'], $this->heroClasses)) {
            throw new Exception("Class isn't valid");
        }
        // Validate Weapon
        if ($this->isValidValue($data['weapon'], $this->heroWeapons)) {
            throw new Exception("Weapon isn't valid");
        }

        // If is random
        if ($data['isRandom']) {
            // Half-orcs and Dragonborns don’t have a Last Name
            if ($data['race'] == "Half-orc" || $data['race'] == "Dragonborn") {
                if ($data['last_name'] != "")
                    throw new Exception($data['race']." can't have last name");
            }

            // If race is Elf, mirror the last name
            if ($data['race'] == "Elf") {
                $data['last_name'] = strrev($data['first_name']);
            }

            // If race is Dwarf, mirror the last name
            if ($data['race'] == "Dwarf") {
                if (strpos(strtolower($data['first_name']), 'r') === false) {
                    if (strpos(strtolower($data['first_name']), 'h') === false) {
                        throw new Exception("First name must have at least one 'r' or 'h'");
                    }
                }
                if (strpos(strtolower($data['last_name']), 'r') === false) {
                    if (strpos(strtolower($data['last_name']), 'h') === false) {
                        throw new Exception("Last name must have at least one 'r' or 'h'");
                    }
                }
            }
        }

        // Class Limitations
        ////////////////////
        // Humans and Half-elves can only be Paladins
        if ($data['race'] == "Human" || $data['race'] == "Half-elf") {
            if ($data['class'] != "Paladin") {
                throw new Exception("Humans and Half-elves can only be Paladins");
            }
        }

        // Dwarfs cannot be Rangers
        if ($data['race'] == "Dwarf") {
            if ($data['class'] == "Ranger") {
                throw new Exception("Dwarfs cannot be Rangers");
            }
        }

        // Elves, Half-elves and Halflings cannot be Barbarians
        if ($data['race'] == "Elf" || $data['race'] == "Half-elf" || $data['race'] == "Halfling") {
            if ($data['class'] == "Barbarian") {
                throw new Exception("Elves, Half-elves and Halflings cannot be Barbarians");
            }
        }

        // Half-orcs and Dwarfs cannot be Wizards
        if ($data['race'] == "Half-orc" || $data['race'] == "Dwarf") {
            if ($data['class'] == "Wizard") {
                throw new Exception("Half-orcs and Dwarfs cannot be Wizards");
            }
        }

        // Dragonborn, and Half-orcs cannot be Clerics
        if ($data['race'] == "Dragonborn" || $data['race'] == "Half-orc") {
            if ($data['class'] == "Cleric") {
                throw new Exception("Dragonborn, and Half-orcs cannot be Clerics");
            }
        }

        // Elfs cannot be Warriors
        if ($data['race'] == "Elf") {
            if ($data['class'] == "Warrior") {
                throw new Exception("Elfs cannot be Warriors");
            }
        }

        // Weapon Limitation
        ////////////////////
        // Paladins can only use Swords
        if ($data['class'] == "Paladin") {
            if ($data['weapon'] != "Sword") {
                throw new Exception("Paladins can only use Swords");
            }
        }

        // Rangers can only use Bow and Arrows
        if ($data['class'] == "Ranger") {
            if ($data['weapon'] != "Bow and Arrows" ) {
                throw new Exception("Rangers can only use Bow and Arrows");
            }
        }

        // Barbarian cannot use Bow and Arrows or Staffs
        if ($data['class'] == "Barbarian") {
            if ($data['weapon'] == "Bow and Arrows" || $data['weapon'] == "Staff" ) {
                throw new Exception("Barbarian cannot use Bow and Arrows or Staffs");
            }
        }

        // Wizards and Clerics can only Use Staffs
        if ($data['class'] == "Wizard" || $data['class'] == "Cleric") {
            if ($data['weapon'] != "Staff") {
                throw new Exception("Wizards and Clerics can only Use Staffs");
            }
        }

        // Thieves cannot use Hammers
        if ($data['class'] == "Thief") {
            if ($data['weapon'] == "Hammer") {
                throw new Exception("Thieves cannot use Hammers");
            }
        }

        // If reach this line, means that the hero object is correct
        return $data;
    }
}
