<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MonsterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListOfMonsters()
    {
        $response = $this->get('/api/monster');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertCount(sizeof($data), $data);
        $response->assertStatus(200);
    }

    public function testShowMonster(){
        $response = $this->get('/api/monster/96548754881');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Can't find the monster", $data['msg']);
        $response = $this->get('/api/monster/1');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Monster information", $data['msg']);
        $response->assertStatus(200);
    }

    public function testDeleteMonster(){
        $response = $this->call('DELETE','/api/monster/1');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Hero deleted", $data['msg']);
    }

    public function testUpdateMonster(){
        $monster['isRandom'] = false;
        $monster['picture'] = "some link manual";
        $monster['name'] = "efefefe name";
        $monster['race'] = "Beholder";
        $monster['stat_strength'] = 0;
        $monster['stat_intelligence'] = 0;
        $monster['stat_dexterity'] = 0;
        $monster['abilities'] = "Shadow Ball";
        $response = $this->json('PUT','/api/monster/2', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Hero updated successfully", $data['msg']);
        $response->assertStatus(200);
    }

    public function testCreateMonsterManually(){
        $monster['isRandom'] = false;
        $monster['picture'] = "some link manual";
        $monster['name'] = "Example name";
        $monster['race'] = "Beholder";
        $monster['abilities'] = "Shadow Ball";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Monster created successfully", $data['msg']);
    }

    public function testCreateMonsterRandom(){

        // Shadow Ball can only be used by Beholder and Mind Flayer
        // According with name formula
        // Beholder should be equal to: "BHE'"
        $monster['isRandom'] = true;
        $monster['picture'] = "some link";
        $monster['race'] = "Beholder";
        $monster['abilities'] = "Shadow Ball";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Monster created successfully", $data['msg']);
        self::assertEquals("BHE'", $data['monster']['name']);
        $monster['race'] = " Gelatinous Cube";
        $monster['abilities'] = "Shadow Ball";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Shadow Ball can only be used by Beholder and Mind Flayer", $data['msg']);

        // Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant, Umber Hulk
        $monster['race'] = " Dragons";
        $monster['abilities'] = "Aerial Ace";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Monster created successfully", $data['msg']);
        $monster['race'] = " Gelatinous Cube";
        $monster['abilities'] = "Aerial Ace";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Aerial Ace can only be used by Owlbear, Dragons, Cloud Giant, Storm Giant, Umber Hulk", $data['msg']);

        // Surf can only be used by Yuan-ti, Gelatinous Cube and Drow
        $monster['race'] = "Gelatinous Cube";
        $monster['abilities'] = "Surf";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Monster created successfully", $data['msg']);

        $monster['race'] = "Dragons";
        $monster['abilities'] = "Surf";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Surf can only be used by Yuan-ti, Gelatinous Cube and Drow", $data['msg']);

        // - Kobold can only use Double Team and Crunch
        $monster['race'] = "Kobold";
        $monster['abilities'] = "Thunderbolt";
        $response = $this->json('POST','/api/monster', $monster);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Kobold can only use Double Team and Crunch", $data['msg']);
    }

    public function heroModel(){
        return $hero = [
            "first_name"=>"Khazun",
            "last_name"=>"something",
            "race"=> "Elf",
            "class"=>"Paladin",
            "weapon"=>"Sword",
            "stat_strength"=>0,
            "stat_intelligence"=>0,
            "stat_dexterity"=>0,
            "isRandom"=> true
        ];
    }

    public function requestCreateHero($hero){
        $response = $this->json('POST','/api/monster', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        return $data["msg"];
    }



}
