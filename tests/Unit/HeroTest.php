<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HeroTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testListOfHeroes()
    {
        $response = $this->get('/api/hero');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertCount(sizeof($data), $data);
        $response->assertStatus(200);
    }

    public function testShowHero(){
        $response = $this->get('/api/hero/965487881');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Can't find the hero", $data['msg']);
        $response = $this->get('/api/hero/1');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Hero information", $data['msg']);
        $response->assertStatus(200);
    }

    public function testDeleteHero(){
        $response = $this->call('DELETE','/api/hero/2');
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Hero deleted", $data['msg']);
        $response->assertStatus(200);
    }

    public function testUpdateHero(){
        $hero = $this->heroModel();
        $response = $this->json('PUT','/api/hero/2', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Hero updated successfully", $data['msg']);
        $response->assertStatus(200);
    }


    public function testCreateHeroManually(){
        // Valid name, class, race and weapon
        $hero = $this->heroModel();
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Hero created successfully", $msg);
    }

    public function testCreateHeroRandom(){
        // Valid name, class, race and weapon
        $hero = $this->heroModel();
        $hero['first_name'] = "zzzzz";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("First name isn't valid", $msg);

        $hero['first_name'] = "Khazun";
        $hero['race'] = "zzzzz";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Race isn't valid", $msg);

        $hero['first_name'] = "Khazun";
        $hero['race'] = "Elf";
        $hero['class'] = "zzzz";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Class isn't valid", $msg);

        $hero['first_name'] = "Khazun";
        $hero['race'] = "Elf";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "zzz";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Weapon isn't valid", $msg);
        $hero['weapon'] = "Sword";

        // - Half-orcs and Dragonborns don’t have a Last Name
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Dragonborn";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Dragonborn can't have last name", $msg);
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Half-orc";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Half-orc can't have last name", $msg);

        //- Elfs’ Last Name must be its First Name, but mirrored (i.e. Jimmy => Ymmij)
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Elf";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Sword";
        $response = $this->json('POST','/api/hero', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("nuzahK", $data["hero"]['last_name']);

        // - Dwarfs First and Last name must contain at least an “R” or an “H”
        $hero['first_name'] = "En";
        $hero['last_name'] = "Nav";
        $hero['race'] = "Dwarf";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Sword";
        $response = $this->json('POST','/api/hero', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("First name must have at least one 'r' or 'h'", $data["msg"]);

        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "Nav";
        $hero['race'] = "Dwarf";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Sword";
        $response = $this->json('POST','/api/hero', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        self::assertEquals("Last name must have at least one 'r' or 'h'", $data["msg"]);

        // - Humans and Half-elves can only be Paladins
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Human";
        $hero['class'] = "Cleric";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Humans and Half-elves can only be Paladins", $msg);
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Half-elf";
        $hero['class'] = "Cleric";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Humans and Half-elves can only be Paladins", $msg);

        //- Dwarfs cannot be Rangers
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Dwarf";
        $hero['class'] = "Ranger";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Dwarfs cannot be Rangers", $msg);

        // - Elves, Half-elves and Halflings cannot be Barbarians
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Elf";
        $hero['class'] = "Barbarian";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Elves, Half-elves and Halflings cannot be Barbarians", $msg);
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Halfling";
        $hero['class'] = "Barbarian";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Elves, Half-elves and Halflings cannot be Barbarians", $msg);
        $hero['first_name'] = "Khazun";
        $hero['last_name'] = "something";
        $hero['race'] = "Half-elf";
        $hero['class'] = "Barbarian";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Humans and Half-elves can only be Paladins", $msg);

        // - Half-orcs and Dwarfs cannot be Wizards
        $hero['last_name'] = "";
        $hero['race'] = "Half-orc";
        $hero['class'] = "Wizard";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Half-orcs and Dwarfs cannot be Wizards", $msg);
        $hero['last_name'] = "Burningsun";
        $hero['race'] = "Dwarf";
        $hero['class'] = "Wizard";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Half-orcs and Dwarfs cannot be Wizards", $msg);

        // - Dragonborn, and Half-orcs cannot be Clerics
        $hero['last_name'] = "";
        $hero['race'] = "Dragonborn";
        $hero['class'] = "Cleric";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Dragonborn, and Half-orcs cannot be Clerics", $msg);
        $hero['last_name'] = "";
        $hero['race'] = "Half-orc";
        $hero['class'] = "Cleric";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Dragonborn, and Half-orcs cannot be Clerics", $msg);

        // - Elfs cannot be Warriors
        $hero['race'] = "Elf";
        $hero['class'] = "Warrior";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Elfs cannot be Warriors", $msg);

        // Paladins can only use Swords
        $hero['race'] = "Human";
        $hero['class'] = "Paladin";
        $hero['weapon'] = "Hammer";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Paladins can only use Swords", $msg);

        // - Rangers can only use Bow and Arrows
        $hero['race'] = "Elf";
        $hero['class'] = "Ranger";
        $hero['weapon'] = "Hammer";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Rangers can only use Bow and Arrows", $msg);

        // - Barbarian cannot use Bow and Arrows or Staffs
        $hero['race'] = "Half-orc";
        $hero['class'] = "Barbarian";
        $hero['weapon'] = "Bow and Arrows";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Barbarian cannot use Bow and Arrows or Staffs", $msg);

        $hero['race'] = "Half-orc";
        $hero['class'] = "Barbarian";
        $hero['weapon'] = "Staff";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Barbarian cannot use Bow and Arrows or Staffs", $msg);

        // - Wizards and Clerics can only Use Staffs
        $hero['race'] = "Elf";
        $hero['class'] = "Wizard";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Wizards and Clerics can only Use Staffs", $msg);
        $hero['race'] = "Elf";
        $hero['class'] = "Cleric";
        $hero['weapon'] = "Sword";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Wizards and Clerics can only Use Staffs", $msg);

        // - Thieves cannot use Hammers
        $hero['race'] = "Elf";
        $hero['class'] = "Thief";
        $hero['weapon'] = "Hammer";
        $msg = $this->requestCreateHero($hero);
        self::assertEquals("Thieves cannot use Hammers", $msg);
    }

    public function heroModel(){
        return $hero = [
            "first_name"=>"Khazun",
            "last_name"=>"something",
            "race"=> "Elf",
            "class"=>"Paladin",
            "weapon"=>"Sword",
            "stat_strength"=>0,
            "stat_intelligence"=>0,
            "stat_dexterity"=>0,
            "isRandom"=> true
        ];
    }



    public function requestCreateHero($hero){
        $response = $this->json('POST','/api/hero', $hero);
        $data = json_decode($response->baseResponse->content(), true);
        return $data["msg"];
    }



}
